/*
 * sftmdiff - string format time difference between two tm structs
 *
 * sftmdiff.h
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file sftmdiff.h
 * @brief String format time difference between two `tm` structs.
 */

#ifndef SFTMDIFF_H_INCLUDED
#define SFTMDIFF_H_INCLUDED

#include <stddef.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Computes the difference between two `tm` structs and provides formatted
 * output of the result.
 *
 * It uses `tmdiff()`, but on copies of `start` and `end`, so they are not
 * modified.
 *
 * Writes at most `maxsize` characters to `strDest`.
 *
 * The return value is the number of characters written, not including the
 * terminating null, or `0` on error.
 *
 * Supported formatting codes:
 *
 *     %%  insert a %
 *     %y  difference in years
 *     %m  difference in months of the year
 *     %w  difference in weeks of the year
 *     %d  difference in days of the month
 *     %H  difference in hours of the day (%0H zero-pad to width 2)
 *     %M  difference in minutes of the hour (%0M zero-pad to width 2)
 *     %S  difference in seconds of the minute (%0S zero-pad to width 2)
 *
 *     %Mw  difference in weeks of the month
 *     %Yd  difference in days of the year
 *     %Wd  difference in days of the week
 *
 *     %#m  total difference in months
 *     %#w  total difference in weeks
 *     %#d  total difference in days
 *     %#H  total difference in hours
 *     %#M  total difference in minutes
 *     %#S  total difference in seconds
 *
 * @param strDest Where to write the string.
 * @param maxsize Max number of characters to write to `strDest`.
 * @param format Format string with formatting codes.
 * @param end End time.
 * @param start Start time.
 * @return Number of characters written, not including the terminating null,
 *   or `0` on error.
 * @see wsftmdiff
 */
size_t sftmdiff(char *strDest,
                size_t maxsize,
                const char *format,
                const struct tm *end,
                const struct tm *start);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* SFTMDIFF_H_INCLUDED */
