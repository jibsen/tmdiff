/*
 * tmdiff - compute time difference between two tm structs
 *
 * tmdiff.h
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file tmdiff.h
 * @brief Compute time difference between two `tm` structs.
 */

#ifndef TMDIFF_H_INCLUDED
#define TMDIFF_H_INCLUDED

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define WEEKSEC (7ul * 24 * 60 * 60)
#define DAYSEC  (24ul * 60 * 60)
#define HOURSEC (60ul * 60)
#define MINSEC  (60ul)

/**
 * Structure used to represent broken-down time difference.
 * @see tmdiff
 */
struct tm_d {
	int year_d; /* difference in years */
	int mon_d;  /* difference in months of the year */
	int mday_d; /* difference in days of the month */
	int yday_d; /* difference in days of the year */
	int hour_d; /* difference in hours of the day */
	int min_d;  /* difference in minutes of the hour */
	int sec_d;  /* difference in seconds of the minute */
};

/**
 * Takes two `tm` structs and computes the difference between them in a `tm_d`
 * struct.
 *
 * Both `start` and `end` must point to `tm` structs that are valid input to
 * `mktime()`. The time represented by `end` must be equal to or after the
 * time represented by `start`.
 *
 * `tmdiff()` calls `mktime()` on both `start` and `end`, so they may be
 * modified in the process.
 *
 * The return value is the total number of seconds between `start` and `end`,
 * or `(unsigned long long)(-1)` on error.
 *
 * If `d` is `NULL`, then only the absolute difference in seconds is computed
 * and returned.
 *
 * The difference in calendar time is `year_d` years, `mon_d` months, `mday_d`
 * days, `hour_d` hours, `min_d` minutes, `sec_d` seconds.
 *
 * The total difference in calendar moths is `12 * year_d + mon_d`.
 *
 * The total difference in clock weeks, days, hours and minutes is the return
 * value divided by the respective macro (`WEEKSEC`, `DAYSEC`, `HOURSEC`,
 * `MINSEC`).
 *
 * The difference in weeks of the year is `yday_d / 7`.
 *
 * The difference in weeks of the month is `mday_d / 7`.
 *
 * The difference in days of the week is `mday_d % 7`.
 *
 * If the calendar time is below one day, `hour_d` is the clock time
 * difference. This means that the day where DST starts may have less than 24
 * hours, and the day it ends more.
 *
 * @param d `tm_d` struct which will receive the time difference.
 * @param end End time.
 * @param start Start time.
 * @return Absolute difference in seconds (as computed by `difftime()`), or
 *   `(unsigned long long)(-1)` on error.
 */
unsigned long long tmdiff(struct tm_d *d, struct tm *end, struct tm *start);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* TMDIFF_H_INCLUDED */
