/*
 * testwsftd - test wide string formatted time difference
 *
 * testwsftd.c
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <wchar.h>
#include <stdio.h>
#include <time.h>

#include "wsftmdiff.h"
#include "wasctmdiff.h"

int main()
{
	wchar_t buffer[1024];
	struct tm start, end;
	time_t now;
	int i;

	/* Jan 31st 2013 00:00:01 */
	start.tm_year = 2013 - 1900;
	start.tm_mon = 0;
	start.tm_mday = 31;
	start.tm_hour = 0;
	start.tm_min = 0;
	start.tm_sec = 1;
	start.tm_isdst = -1;

	/* Mar 1st 2013 00:00:01 */
	end.tm_year = 2013 - 1900;
	end.tm_mon = 2;
	end.tm_mday = 1;
	end.tm_hour = 0;
	end.tm_min = 0;
	end.tm_sec = 1;
	end.tm_isdst = -1;

	wsftmdiff(buffer, 1024, L"%y years %m months %d days %0H:%0M:%0S", &end, &start);

	wprintf(L"'%s'\n", buffer);

	/* Jul 2nd 2013 13:30:00 */
	start.tm_year = 2013 - 1900;
	start.tm_mon = 6;
	start.tm_mday = 2;
	start.tm_hour = 13;
	start.tm_min = 30;
	start.tm_sec = 0;
	start.tm_isdst = -1;

	/* Aug 1st 2013 13:30:00 */
	end.tm_year = 2013 - 1900;
	end.tm_mon = 7;
	end.tm_mday = 1;
	end.tm_hour = 13;
	end.tm_min = 30;
	end.tm_sec = 0;
	end.tm_isdst = -1;

	wsftmdiff(buffer, 1024, L"%y years %m months %d days %0H:%0M:%0S", &end, &start);

	wprintf(L"'%s'\n", buffer);

	/* Aug 28th 2011 13:30:54 */
	start.tm_year = 2011 - 1900;
	start.tm_mon = 7;
	start.tm_mday = 28;
	start.tm_hour = 13;
	start.tm_min = 30;
	start.tm_sec = 54;
	start.tm_isdst = -1;

	/* Mar 1st 2013 04:15:00 */
	end.tm_year = 2013 - 1900;
	end.tm_mon = 2;
	end.tm_mday = 1;
	end.tm_hour = 4;
	end.tm_min = 15;
	end.tm_sec = 0;
	end.tm_isdst = -1;

	wsftmdiff(buffer, 1024, L"%y years %m months %d days %0H:%0M:%0S", &end, &start);

	wprintf(L"'%s'\n\n", buffer);

	/* get current time */
	time(&now);
	start = *localtime(&now);
	end = start;

	wprintf(L"\n--- 15 minute steps ---\n");

	for (i = 0; i < 10; ++i) {
		end.tm_min += 15;
		end.tm_isdst = -1;
		mktime(&end);

		wasctmdiff(buffer, 1024, &end, &start, 2, 0, NULL, NULL);

		wprintf(L"'%s'\n", buffer);
	}

	wprintf(L"\n--- 6 hour steps ---\n");
	end = start;

	for (i = 0; i < 10; ++i) {
		end.tm_hour += 6;
		end.tm_isdst = -1;
		mktime(&end);

		wasctmdiff(buffer, 1024, &end, &start, 2, 0, NULL, NULL);

		wprintf(L"'%s'\n", buffer);
	}

	wprintf(L"\n--- 2 day steps ---\n");
	end = start;

	for (i = 0; i < 10; ++i) {
		end.tm_mday += 2;
		end.tm_isdst = -1;
		mktime(&end);

		wasctmdiff(buffer, 1024, &end, &start, 2, 0, NULL, NULL);

		wprintf(L"'%s'\n", buffer);
	}

	wprintf(L"\n--- 11 day steps ---\n");
	end = start;

	for (i = 0; i < 10; ++i) {
		end.tm_mday += 11;
		end.tm_isdst = -1;
		mktime(&end);

		wasctmdiff(buffer, 1024, &end, &start, 2, 0, NULL, NULL);

		wprintf(L"'%s'\n", buffer);
	}

	wprintf(L"\n--- 3 month steps ---\n");
	end = start;

	for (i = 0; i < 10; ++i) {
		end.tm_mon += 3;
		end.tm_isdst = -1;
		mktime(&end);

		wasctmdiff(buffer, 1024, &end, &start, 2, 0, NULL, NULL);

		wprintf(L"'%s'\n", buffer);
	}

	return 0;
}
