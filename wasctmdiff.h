/*
 * wasctmdiff - convert time difference between two tm structs to a string
 *
 * wasctmdiff.h
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file wasctmdiff.h
 * @brief Convert time difference between two `tm` structs to a wide string.
 */

#ifndef WASCTMDIFF_H_INCLUDED
#define WASCTMDIFF_H_INCLUDED

#include <wchar.h>
#include <stddef.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Computes the difference between two `tm` structs and creates a string with
 * a textual representation.
 *
 * It uses `tmdiff()`, but on copies of `start` and `end`, so they are not
 * modified.
 *
 * Writes at most `maxsize` characters to `strDest`.
 *
 * The possible fields used are year, month, week, day, hour, minute, and
 * second. At most `max_fields` consecutive fields are used in the output
 * string. So if `max_fields` is 3 and the first field used is month, then
 * only week and day are potentially used as well.
 *
 * If `allow_convert` is non-zero, the first field used may be converted down
 * to the next if the value is relatively small. For instance if the first
 * field would be year and the difference is 18 months or less, then the year
 * is output as months instead.
 *
 * If the seconds field is used, it is included even if the difference is 0 to
 * avoid a large shift in the length of the string.
 *
 * `str_p` and `str_s` are double-null terminated strings containing the 7
 * strings to be used for the fields. `str_p` is the plural, `str_s` the
 * singular forms. If `NULL`, default long English strings are used.
 *
 * The return value is the number of characters written, not including the
 * terminating null, or `0` on error.
 *
 * @param strDest Where to write the string.
 * @param maxsize Max number of characters to write to `strDest`.
 * @param end End time.
 * @param start Start time.
 * @param max_fields Max number of consecutive fields used in the output.
 * @param allow_convert If non-zero, the first field used may be converted
 *   down to the next if the value is relatively small.
 * @param str_p Double-null terminated string containint 7 strings in plural
 *   to be used for the fields. If `NULL`, default English strings are used.
 * @param str_s Double-null terminated string containint 7 strings in singular
 *   to be used for the fields. If `NULL`, default English strings are used.
 * @return Number of characters written, not including the terminating null,
 *   or `0` on error.
 * @see asctmdiff
 */
size_t wasctmdiff(wchar_t *strDest,
                  size_t maxsize,
                  const struct tm *end,
                  const struct tm *start,
                  int max_fields,
                  int allow_convert,
                  const wchar_t *str_p,
                  const wchar_t *str_s);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* WASCTMDIFF_H_INCLUDED */
