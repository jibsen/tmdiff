/*
 * tmdiff - compute time difference between two tm structs
 *
 * tmdiff.c
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stddef.h>
#include <assert.h>

#include "tmdiff.h"

/* returns 1 if y is a leap year, 0 otherwise */
static int leap(int y)
{
	return (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)) ? 1 : 0;
}

unsigned long long tmdiff(struct tm_d *d, struct tm *end, struct tm *start)
{
	const int md[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	unsigned long long seconds;

	int year_borrow = 0;
	int mon_borrow = 0;
	int day_borrow = 0;
	int hour_borrow = 0;
	int min_borrow = 0;

	time_t start_time, end_time;

	assert(end != NULL);
	assert(start != NULL);

	/* convert tm structures to time_t values */
	start_time = mktime(start);
	end_time = mktime(end);

	/* check for error */
	if (start_time == (time_t) (-1) || end_time == (time_t) (-1)) {
		return (unsigned long long) (-1);
	}

	/* compute the difference in seconds between those two time_t values */
	seconds = (unsigned long long) difftime(end_time, start_time);

	if (d == NULL) {
		return seconds;
	}

	d->year_d = 0;
	d->mon_d = 0;
	d->mday_d = 0;
	d->yday_d = 0;
	d->hour_d = 0;
	d->min_d = 0;
	d->sec_d = 0;

	/* difference in the seconds */
	d->sec_d = end->tm_sec - start->tm_sec;

	/* if negative, we have to borrow a minute */
	if (d->sec_d < 0) {
		d->sec_d = 60 + d->sec_d;
		min_borrow = 1;
	}

	/* difference in the minutes */
	d->min_d = end->tm_min - start->tm_min - min_borrow;

	/* if negative, we have to borrow an hour */
	if (d->min_d < 0) {
		d->min_d = 60 + d->min_d;
		hour_borrow = 1;
	}

	/* difference in the hours */
	d->hour_d = end->tm_hour - start->tm_hour - hour_borrow;

	/* if negative, we have to borrow a day */
	if (d->hour_d < 0) {
		d->hour_d = 24 + d->hour_d;
		day_borrow = 1;
	}

	/* difference in the days of year */
	d->yday_d = end->tm_yday - start->tm_yday - day_borrow;

	/* if negative, we have to borrow a year */
	if (d->yday_d < 0) {
		d->yday_d = 365 + leap(1900 + start->tm_year) + d->yday_d;
	}

	/* difference in the days of month */
	d->mday_d = end->tm_mday - start->tm_mday - day_borrow;

	/* if negative, we have to borrow a month */
	if (d->mday_d < 0) {
		int start_mon_days;

		assert(start->tm_mon >= 0 && start->tm_mon < 12);

		/* get number of days in start month */
		start_mon_days = md[start->tm_mon];

		/* if february, correct for leap year */
		if (start->tm_mon == 1) {
			start_mon_days += leap(1900 + start->tm_year);
		}

		d->mday_d = start_mon_days + d->mday_d;
		mon_borrow = 1;
	}

	/* difference in the months */
	d->mon_d = end->tm_mon - start->tm_mon - mon_borrow;

	/* if negative, we have to borrow a year */
	if (d->mon_d < 0) {
		d->mon_d = 12 + d->mon_d;
		year_borrow = 1;
	}

	/* difference in the years */
	d->year_d = end->tm_year - start->tm_year - year_borrow;

	/* if difference is below one calendar day and there was a DST difference
	   we adjust hour difference to clock time */
	if (d->year_d + d->mon_d + d->mday_d == 0 && d->hour_d != (seconds % DAYSEC) / HOURSEC) {
		int hours = d->hour_d;

		d->hour_d = (seconds % DAYSEC) / HOURSEC;

		/* handle the special case where DST increased hours past 24 */
		if (hours - d->hour_d > 11) {
			d->hour_d += 24;
		}
	}

	/* return total difference in seconds */
	return seconds;
}
