/*
 * wsftmdiff - wide string format time difference between two tm structs
 *
 * wsftmdiff.c
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <wchar.h>
#include <stddef.h>
#include <assert.h>

#include "wsftmdiff.h"
#include "tmdiff.h"

struct wsftmd_state {
	wchar_t *d;
	size_t left;
};

static int wsftmd_print_number(struct wsftmd_state *state, unsigned long long v, int zero_flag)
{
	wchar_t rev[64];
	unsigned int len = 0;
	unsigned int i;

	/* handle single-digit values who might need a zero in front of them */
	if (v < 10) {
		rev[len++] = L'0' + (wchar_t) v;
		if (zero_flag) {
			rev[len++] = L'0';
		}
	}
	else {
		/* put digits of v into rev in reverse order */
		while (v > 0) {
			unsigned int digit;

			digit = v % 10;
			v /= 10;

			rev[len++] = L'0' + digit;
		}
	}

	/* check there is room for all digits */
	if (state->left < len) {
		return 0;
	}

	state->left -= len;

	/* copy digits into destination string */
	for (i = len; i; --i) {
		*state->d++ = rev[i - 1];
	}

	return len;
}

size_t wsftmdiff(wchar_t *strDest, size_t maxsize, const wchar_t *format, const struct tm *end, const struct tm *start)
{
	struct wsftmd_state state;
	const wchar_t *p;
	unsigned long long seconds;
	struct tm tmstart = *start;
	struct tm tmend = *end;
	struct tm_d tmd;

	assert(strDest != NULL);
	assert(format != NULL);
	assert(end != NULL);
	assert(start != NULL);

	if (maxsize == 0) {
		return 0;
	}

	/* get time difference */
	seconds = tmdiff(&tmd, &tmend, &tmstart);

	if (seconds == (time_t) (-1)) {
		return 0;
	}

	state.d = strDest;
	state.left = maxsize;

	/* loop through format string */
	for (p = format; *p != L'\0'; ++p) {
		/* check if we are at the start of a formatting code */
		if (*p == L'%') {
			unsigned long long v;
			int total_flag = 0;
			int zero_flag = 0;

			++p;

			/* %% inserts a literal % */
			if (*p == L'%') {
				if (state.left-- == 0) {
					return 0;
				}
				*state.d++ = *p;
				continue;
			}

			/* # is total flag */
			if (*p == L'#') {
				total_flag = 1;
				++p;
			}

			/* 0 is zero flag */
			if (*p == L'0') {
				zero_flag = 1;
				++p;
			}

			/* check for two character formatting codes, Mw Yd Wd */
			switch (*p) {
			case L'M':
				if (p[1] == L'w') {
					++p;
					if (!wsftmd_print_number(&state, total_flag ? seconds / WEEKSEC : tmd.mday_d / 7u, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			case L'W':
				if (p[1] == L'd') {
					++p;
					if (!wsftmd_print_number(&state, total_flag ? seconds / DAYSEC : tmd.mday_d % 7u, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			case L'Y':
				if (p[1] == L'd') {
					++p;
					if (!wsftmd_print_number(&state, total_flag ? seconds / DAYSEC : tmd.yday_d, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			}

			/* check for one character formatting codes, y m w d H M S */
			switch (*p) {
			case L'y':
				v = tmd.year_d;
				break;
			case L'm':
				v = total_flag ? 12 * tmd.year_d + tmd.mon_d : tmd.mon_d;
				break;
			case L'w':
				v = total_flag ? seconds / WEEKSEC : tmd.yday_d / 7u;
				break;
			case L'd':
				v = total_flag ? seconds / DAYSEC : tmd.mday_d;
				break;
			case L'H':
				v = total_flag ? seconds / HOURSEC : tmd.hour_d;
				break;
			case L'M':
				v = total_flag ? seconds / MINSEC : tmd.min_d;
				break;
			case L'S':
				v = total_flag ? seconds : tmd.sec_d;
				break;
			default:
				/* default to just copying the character and continue loop at next char */
				if (state.left-- == 0) {
					return 0;
				}
				*state.d++ = *p;
				continue;
			}

			/* print number */
			if (!wsftmd_print_number(&state, v, zero_flag)) {
				return 0;
			}
		}
		else {
			/* not a formatting code, so just copy character */
			if (state.left-- == 0) {
				return 0;
			}
			*state.d++ = *p;
		}
	}

	/* zero-terminate string */
	if (state.left-- == 0) {
		return 0;
	}
	*state.d++ = L'\0';

	/* return length without zero-terminator */
	return state.d - strDest - 1;
}
