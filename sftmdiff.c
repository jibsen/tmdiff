/*
 * sftmdiff - string format time difference between two tm structs
 *
 * sftmdiff.c
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stddef.h>
#include <assert.h>

#include "sftmdiff.h"
#include "tmdiff.h"

struct sftmd_state {
	char *d;
	size_t left;
};

static int sftmd_print_number(struct sftmd_state *state, unsigned long long v, int zero_flag)
{
	unsigned char rev[64];
	unsigned int len = 0;
	unsigned int i;

	/* handle single-digit values who might need a zero in front of them */
	if (v < 10) {
		rev[len++] = '0' + (unsigned char) v;
		if (zero_flag) {
			rev[len++] = '0';
		}
	}
	else {
		/* put digits of v into rev in reverse order */
		while (v > 0) {
			unsigned int digit;

			digit = v % 10;
			v /= 10;

			rev[len++] = '0' + digit;
		}
	}

	/* check there is room for all digits */
	if (state->left < len) {
		return 0;
	}

	state->left -= len;

	/* copy digits into destination string */
	for (i = len; i; --i) {
		*state->d++ = rev[i - 1];
	}

	return len;
}

size_t sftmdiff(char *strDest, size_t maxsize, const char *format, const struct tm *end, const struct tm *start)
{
	struct sftmd_state state;
	const char *p;
	unsigned long long seconds;
	struct tm tmstart = *start;
	struct tm tmend = *end;
	struct tm_d tmd;

	assert(strDest != NULL);
	assert(format != NULL);
	assert(end != NULL);
	assert(start != NULL);

	if (maxsize == 0) {
		return 0;
	}

	/* get time difference */
	seconds = tmdiff(&tmd, &tmend, &tmstart);

	if (seconds == (time_t) (-1)) {
		return 0;
	}

	state.d = strDest;
	state.left = maxsize;

	/* loop through format string */
	for (p = format; *p; ++p) {
		/* check if we are at the start of a formatting code */
		if (*p == '%') {
			unsigned long long v;
			int total_flag = 0;
			int zero_flag = 0;

			++p;

			/* %% inserts a literal % */
			if (*p == '%') {
				if (state.left-- == 0) {
					return 0;
				}
				*state.d++ = *p;
				continue;
			}

			/* # is total flag */
			if (*p == '#') {
				total_flag = 1;
				++p;
			}

			/* 0 is zero flag */
			if (*p == '0') {
				zero_flag = 1;
				++p;
			}

			/* check for two character formatting codes, Mw Yd Wd */
			switch (*p) {
			case 'M':
				if (p[1] == 'w') {
					++p;
					if (!sftmd_print_number(&state, total_flag ? seconds / WEEKSEC : tmd.mday_d / 7u, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			case 'W':
				if (p[1] == 'd') {
					++p;
					if (!sftmd_print_number(&state, total_flag ? seconds / DAYSEC : tmd.mday_d % 7u, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			case 'Y':
				if (p[1] == 'd') {
					++p;
					if (!sftmd_print_number(&state, total_flag ? seconds / DAYSEC : tmd.yday_d, zero_flag)) {
						return 0;
					}
					continue;
				}
				break;
			}

			/* check for one character formatting codes, y m w d H M S */
			switch (*p) {
			case 'y':
				v = tmd.year_d;
				break;
			case 'm':
				v = total_flag ? 12 * tmd.year_d + tmd.mon_d : tmd.mon_d;
				break;
			case 'w':
				v = total_flag ? seconds / WEEKSEC : tmd.yday_d / 7u;
				break;
			case 'd':
				v = total_flag ? seconds / DAYSEC : tmd.mday_d;
				break;
			case 'H':
				v = total_flag ? seconds / HOURSEC : tmd.hour_d;
				break;
			case 'M':
				v = total_flag ? seconds / MINSEC : tmd.min_d;
				break;
			case 'S':
				v = total_flag ? seconds : tmd.sec_d;
				break;
			default:
				/* default to just copying the character and continue loop at next char */
				if (state.left-- == 0) {
					return 0;
				}
				*state.d++ = *p;
				continue;
			}

			/* print number */
			if (!sftmd_print_number(&state, v, zero_flag)) {
				return 0;
			}
		}
		else {
			/* not a formatting code, so just copy character */
			if (state.left-- == 0) {
				return 0;
			}
			*state.d++ = *p;
		}
	}

	/* zero-terminate string */
	if (state.left-- == 0) {
		return 0;
	}
	*state.d++ = '\0';

	/* return length without zero-terminator */
	return state.d - strDest - 1;
}
