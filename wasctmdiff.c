/*
 * wasctmdiff - convert time difference between two tm structs to a string
 *
 * wasctmdiff.c
 *
 * Copyright 2012-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <wchar.h>
#include <stddef.h>
#include <assert.h>

#include "wasctmdiff.h"
#include "tmdiff.h"

struct watmd_state {
	wchar_t *d;
	size_t left;
};

static int watmd_print_field(struct watmd_state *state, int first, int v, const wchar_t *str_p, const wchar_t *str_s)
{
	wchar_t rev[64];
	unsigned int len = 0;
	unsigned int num = 0;
	const wchar_t *str;

	/* select singular or plural string based on value */
	str = (v == 1) ? str_s : str_p;

	/* if not first field, add a space */
	if (!first) {
		if (state->left-- == 0) {
			return 0;
		}
		*state->d++ = L' ';
		++len;
	}

	/* put digits of v into rev in reverse order */
	if (v <= 0) {
		rev[num++] = L'0';
	}
	else {
		while (v > 0) {
			unsigned int digit;

			digit = v % 10;
			v /= 10;

			rev[num++] = L'0' + digit;
		}
	}

	/* check there is room for all digits */
	if (state->left < num) {
		return 0;
	}

	len += num;
	state->left -= num;

	/* copy digits into destination string */
	for (; num; --num) {
		*state->d++ = rev[num - 1];
	}

	/* add a space */
	if (state->left-- == 0) {
		return 0;
	}
	*state->d++ = L' ';
	++len;

	/* add field string to destination string */
	while (*str != L'\0') {
		if (state->left-- == 0) {
			return 0;
		}
		*state->d++ = *str++;
		++len;
	}

	return len;
}

size_t wasctmdiff(wchar_t *strDest, size_t maxsize, const struct tm *end, const struct tm *start, int max_fields, int allow_convert, const wchar_t *str_p, const wchar_t *str_s)
{
	const wchar_t *default_str_p = L"years\0months\0weeks\0days\0hours\0minutes\0seconds\0\0";
	const wchar_t *default_str_s = L"year\0month\0week\0day\0hour\0minute\0second\0\0";
	struct watmd_state state;
	struct tm tmstart = *start;
	struct tm tmend = *end;
	struct tm_d tmd;
	int num_fields = 0;
	int first = 1;

	/* use default strings if none supplied */
	if (str_p == NULL) {
		str_p = default_str_p;
	}
	if (str_s == NULL) {
		str_s = default_str_s;
	}

	if (maxsize == 0) {
		return 0;
	}

	/* get time difference */
	if (tmdiff(&tmd, &tmend, &tmstart) == (time_t) (-1)) {
		return 0;
	}

	state.d = strDest;
	state.left = maxsize;

	/* years */
	if (num_fields < max_fields && tmd.year_d) {
		/* if 18 months or less, output years as months */
		if (first && allow_convert && 12 * tmd.year_d + tmd.mon_d <= 18) {
			tmd.mon_d += 12 * tmd.year_d;
		}
		else {
			if (!watmd_print_field(&state, first, tmd.year_d, str_p, str_s)) {
				return 0;
			}
			first = 0;
		}
	}

	/* if we started output, increment number of fields passed */
	if (!first) {
		num_fields++;
	}
	/* advance str_p and str_s to the next strings */
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* months */
	if (num_fields < max_fields && tmd.mon_d) {
		/* if less than 2 months, output months as weeks */
		if (first && allow_convert && tmd.mon_d < 2) {
			/* since weeks are computed as mday_d / 7 below, we set mday_d
			   to the difference in days of the year to get the proper
			   difference in weeks of the year below */
			tmd.mday_d = tmd.yday_d;
		}
		else {
			if (!watmd_print_field(&state, first, tmd.mon_d, str_p, str_s)) {
				return 0;
			}
			first = 0;
		}
	}

	if (!first) {
		num_fields++;
	}
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* weeks */
	if (num_fields < max_fields && tmd.mday_d / 7) {
		/* if 3 weeks or less, output weeks as days */
		if (!(first && allow_convert && tmd.mday_d <= 21)) {
			if (!watmd_print_field(&state, first, tmd.mday_d / 7, str_p, str_s)) {
				return 0;
			}
			first = 0;
			/* adjust mday_d to be the remaining days */
			tmd.mday_d %= 7;
		}
	}

	if (!first) {
		num_fields++;
	}
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* days */
	if (num_fields < max_fields && tmd.mday_d) {
		/* if 48 hours of less, output days as hours */
		if (first && allow_convert && 24 * tmd.mday_d + tmd.hour_d <= 48) {
			tmd.hour_d += 24 * tmd.mday_d;
		}
		else {
			if (!watmd_print_field(&state, first, tmd.mday_d, str_p, str_s)) {
				return 0;
			}
			first = 0;
		}
	}

	if (!first) {
		num_fields++;
	}
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* hours */
	if (num_fields < max_fields && tmd.hour_d) {
		/* if 90 minutes or less, output hours as minutes */
		if (first && allow_convert && 60 * tmd.hour_d + tmd.min_d <= 90) {
			tmd.min_d += 60 * tmd.hour_d;
		}
		else {
			if (!watmd_print_field(&state, first, tmd.hour_d, str_p, str_s)) {
				return 0;
			}
			first = 0;
		}
	}

	if (!first) {
		num_fields++;
	}
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* minutes */
	if (num_fields < max_fields && tmd.min_d) {
		if (!watmd_print_field(&state, first, tmd.min_d, str_p, str_s)) {
			return 0;
		}
		first = 0;
	}

	if (!first) {
		num_fields++;
	}
	while (*str_p++) {
		continue;
	}
	while (*str_s++) {
		continue;
	}

	/* we print seconds even if zero to avoid large shifts in the size of
	   the string */
	if (num_fields < max_fields) {
		if (!watmd_print_field(&state, first, tmd.sec_d, str_p, str_s)) {
			return 0;
		}
		first = 0;
	}

	/* zero-terminate string */
	if (state.left-- == 0) {
		return 0;
	}
	*state.d++ = L'\0';

	/* return length without zero-terminator */
	return state.d - strDest - 1;
}
